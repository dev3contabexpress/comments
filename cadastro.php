<?php 
require("formulario.php");
?>
<body>
<div class="container" id="comments" style="display: none;">
    <form method="post" id="cadComentario" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="row">
            <div class="col-25">
            <label for="nome">Nome:</label>
            </div>
            <div class="col-75">
            <span class="error"><?php echo $nomeErr;?></span> 
            <input type="text" id="nome" name="nome" placeholder="Seu nome..">
             </div>
        </div>
        <div class="row">
            <div class="col-25">
            <label for="email">E-mail:</label>
            </div>
            <div class="col-75">
            <span class="error"><?php echo $emailErr;?></span> 
            <input type="text" id="email" name="email" placeholder="Seu email..">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
            <label for="comentario">Comentário:</label>
            </div>
            <div class="col-75">
            <span class="error"><?php echo $comentarioErr;?></span> 
            <textarea id="comentario" name="comentario" placeholder="Escreva alguma coisa.." style="height:200px"></textarea>
            </div>
        </div>
        <div class="row">
            <input type="submit" id="btCadastro" value="Cadastrar">
        </div>
    </form> 
</div> 
</body>