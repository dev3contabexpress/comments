<?php 
require("src/config.php"); 
$nome = $email = $comentario = "";
$nomeErr = $emailErr = $comentarioErr = "";
$nomeAcc = $emailAcc = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["nome"])) {
        $nomeErr = "Nome é obrigatório";
    } else {
        $nome = test_input($_POST["nome"]);
        if (!preg_match("/^[a-zA-Z-' ]*$/",$nome)) {
            $nomeErr = "Somente letras e espaços em branco são permitidos";
        } 
        else {
            $nomeAcc = $nome;
        }
    } 

    if (empty($_POST["email"])) {
        $emailErr = "email é obrigatório";
    } else {
        $email = test_input($_POST["email"]); 
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Formato invalido";
        }
        else {
            $emailAcc = $email;
        }
    }

    if (empty($_POST["comentario"])) {
        $comentarioErr = "comentario é obrigatório";
    } else {
        $comentario = test_input($_POST["comentario"]);
    }

    if ($nomeAcc and $emailAcc and $comentario) {
        cadastrar($nomeAcc, $emailAcc, $comentario); 
        echo "Cadastrado com sucesso!!!";
    }
    else {
        echo "Erro";
    }
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>