<link rel="stylesheet" href="layouts/style.css">
<body>
    <h2>Comentarios</h2>
    <div class="container">
        <?php 
        $comments = mostraTodos();
        foreach($comments as $comment) : ?> 
            <div class="box">
                <dl>
                    <dt class="nome"> <?= $comment["nome"] ?> </dt>
                    <dd class="comentario"> <?= $comment["comentario"] ?></dd>
                </dl>
            </div>
        <?php endforeach; ?> 
    </div>
</body> 