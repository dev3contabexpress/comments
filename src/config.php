<?php
function getConnection() {
    $host = "127.0.0.1";
    $user = "dbteste";
    $password = "123456";
    $dbname = "db_comments"; 
    try { 
        $conn = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }catch(PDOException $e) {
        echo "Conexão falhou: " . $e->getMessage();
    } 
    $conn = null;
} 

function mostraTodos() {
    $sql = "SELECT * FROM comment";
    $stmt = getConnection()->prepare($sql);
    $stmt->execute(); 
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function cadastrar($nome, $email, $comentario) {
    $sql = "INSERT INTO comment (nome, email, comentario) 
    VALUES ('$nome', '$email', '$comentario')";
    getConnection()->exec($sql); 
}